Reproduce errors
==========



```bash

git clone https://gitlab.com/theSage21/mypack

cd mypack
pipenv install -e . --verbose
```

Notice that it does not even search in gitlab.

```bash
pipenv shell
python setup.py install
```

This works ok.


What am I doing wrong?
