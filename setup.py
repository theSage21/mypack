from setuptools import setup

__version__ = '0.1'

setup(name='mypack',
      version=__version__,
      description='mypack',
      url='http://gitlab.com/thesage21/mypack',
      author='Arjoonn Sharma',
      author_email='arjoonn.94@gmail.com',
      install_requires=['otherpack==0.1'],
      dependency_links=['git+ssh://git@gitlab.com/theSage21/otherpack#egg=otherpack-0.1'],
      packages=['mypack'],
      keywords=['mypack'],
      zip_safe=False)
